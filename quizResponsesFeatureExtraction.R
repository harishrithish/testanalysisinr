library('sqldf')
library('anytime')

#extract num of attempts per question
numAttemptsPerQn <- sqldf( sprintf("SELECT questionId FROM quizResponsesDF") )

numAttemptsPerQn <- data.frame(table(numAttemptsPerQn))
colnames(numAttemptsPerQn) <- c("questionId","NumOfAttempts")

#extract first attempt correct option ratio
firstAttemptCorrectOptionRatio <- lapply(questionList, function(question) {
  correctAnswers <- sqldf( sprintf("SELECT questionId FROM quizResponsesDF WHERE 
                                          optionOutcome='Correct' AND 
                                          attemptNo=1 AND 
                                          questionId='%s'",question) )
  
  allAnswers <- sqldf( sprintf("SELECT questionId FROM quizResponsesDF WHERE 
                                          attemptNo=1 AND 
                                          questionId='%s'",question) )
  ratio <- nrow(correctAnswers)/nrow(allAnswers)
  ratioFeatureList = list()
  ratioFeatureList[[1]] <- ratio
  names(ratioFeatureList) <- c(question)
  return(ratioFeatureList)
})

firstAttemptCorrectOptionRatio <- unlist(firstAttemptCorrectOptionRatio)
firstAttemptCorrectOptionRatio <- data.frame(firstAttemptCorrectOptionRatio)

#extract last attempt correct option ratio
lastAttemptCorrectOptionRatio <- lapply(questionList, function(question) {
  correctAnswers <- sqldf( sprintf("SELECT questionId FROM quizResponsesDF WHERE 
                                          optionOutcome='Correct' AND 
                                          lastAttempt='TRUE' AND 
                                          questionId='%s'",question) )
  
  allAnswers <- sqldf( sprintf("SELECT questionId FROM quizResponsesDF WHERE 
                                          lastAttempt='TRUE' AND 
                                          questionId='%s'",question) )
  ratio <- nrow(correctAnswers)/nrow(allAnswers)
  ratioFeatureList = list()
  ratioFeatureList[[1]] <- ratio
  names(ratioFeatureList) <- c(question)
  return(ratioFeatureList)
})

lastAttemptCorrectOptionRatio <- unlist(lastAttemptCorrectOptionRatio)
lastAttemptCorrectOptionRatio <- data.frame(lastAttemptCorrectOptionRatio)

#extract all attempts correct option ratio
allAttemptsCorrectOptionRatio <- lapply(questionList, function(question) {
  correctAnswers <- sqldf( sprintf("SELECT questionId FROM quizResponsesDF WHERE 
                                          optionOutcome='Correct' AND 
                                          questionId='%s'",question) )
  
  allAnswers <- sqldf( sprintf("SELECT questionId FROM quizResponsesDF WHERE 
                                          questionId='%s'",question) )
  ratio <- nrow(correctAnswers)/nrow(allAnswers)
  ratioFeatureList = list()
  ratioFeatureList[[1]] <- ratio
  names(ratioFeatureList) <- c(question)
  return(ratioFeatureList)
})

allAttemptsCorrectOptionRatio <- unlist(allAttemptsCorrectOptionRatio)
allAttemptsCorrectOptionRatio <- data.frame(allAttemptsCorrectOptionRatio)

#extract number of correct responses per question
numCorrectAnswersPerQn <- sqldf( sprintf("SELECT questionId FROM quizResponsesDF WHERE optionOutcome='Correct' AND 
                                                        lastAttempt='TRUE'") )

numCorrectAnswersPerQn <- data.frame(table(numCorrectAnswersPerQn))
colnames(numCorrectAnswersPerQn) <- c("questionId","NumOfCorrectResponses")

#extract time spent on questions
timeSpentOnQuestions <- lapply(questionList,function(question){
  qnList <- c(0)
  names(qnList) <- c(question)
  return(qnList)
})
timeSpentOnQuestions <- unlist(timeSpentOnQuestions)

timespentDF <- data.frame()

for( user in quizResponses ){
  prevTime <- anytime(user$created)
  qnTimestamp <- lapply(user$answers,function(answer) {
    qnTimestamp <- c("questionId"=answer$questionId,"timestamp"=answer$timestamp)
    return(qnTimestamp)
  })
  timestamps <- sapply(qnTimestamp,'[[','timestamp')
  qnTimestamp <- qnTimestamp[order(timestamps)]
  
  for(answer in qnTimestamp) {
    timeDiff <- as.numeric(difftime(anytime(answer[["timestamp"]]),
                                    prevTime,units = 'mins'),units='mins')
    prevTime = anytime(answer[["timestamp"]])
    timespentDF <- as.data.frame.matrix(rbind(timespentDF,c(answer[["questionId"]],timeDiff)))
    timeSpentOnQuestions[[answer[["questionId"]]]] = timeSpentOnQuestions[[answer[["questionId"]]]]+timeDiff
  }
}
colnames(timespentDF) <- c("questionId","timespent")

#extract other response features
responseFeatures <- lapply(questionList, function(question) {
  timespentPerQuestion <- sqldf( sprintf("SELECT timespent FROM timespentDF WHERE 
                                          questionId='%s'",question) )
  mu <- mean(as.numeric(timespentPerQuestion[,"timespent"]))
  sigma <- sd(as.numeric(timespentPerQuestion[,"timespent"]))
  variance <- var(as.numeric(timespentPerQuestion[,"timespent"]))
  
  responseFeatures <- c("mean"=mu,"deviation"=sigma,"variance"=variance)
  responseFeaturesList = list()
  responseFeaturesList[[1]] <- responseFeatures
  names(responseFeaturesList) <- c(question)
  return(responseFeaturesList)
})

responseFeatures <- unlist(responseFeatures,recursive = FALSE)
responseFeatures <- as.data.frame.matrix(t(data.frame(responseFeatures)))
rownames(responseFeatures) <- questionList

responseFeatures <- cbind(responseFeatures, "NumOfAttempts"=numAttemptsPerQn[,"NumOfAttempts"])
responseFeatures <- cbind(responseFeatures, "firstAttemptCorrectOptionRatio"=firstAttemptCorrectOptionRatio[,"firstAttemptCorrectOptionRatio"])
responseFeatures <- cbind(responseFeatures, "lastAttemptCorrectOptionRatio"=lastAttemptCorrectOptionRatio[,"lastAttemptCorrectOptionRatio"])
responseFeatures <- cbind(responseFeatures, "allAttemptsCorrectOptionRatio"=allAttemptsCorrectOptionRatio[,"allAttemptsCorrectOptionRatio"])
responseFeatures <- cbind("NumOfCorrectResponses"=numCorrectAnswersPerQn[,"NumOfCorrectResponses"],responseFeatures)

rm(allAttemptsCorrectOptionRatio,firstAttemptCorrectOptionRatio,lastAttemptCorrectOptionRatio,
   numAttemptsPerQn,timespentDF,numCorrectAnswersPerQn)

